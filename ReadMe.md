# Symfony - Blog : UE 222, CVTIC Limoges

**Aymeric Leger Achard**

La configuration du docker-compose a été modifiée
pour intégrer directement le serveur LAMP de CVTIC.

De plus le fichier :

**docker/sites-available/000-default.conf**

est automatiquement copier dans le chemin :

**/etc/apache2/sites-available/**

Ce qui a pour effet de limiter la configuration à faire.

---

## Installation

Exécuter le **docker-compose.yml** via Docker.

Après sa création, entrer dans le terminal du container "server".

Saisir :

``./installEnv.sh``

Cela aura pour effet :

- De mettre à jour l'OS
- D'installer NPM
- De mettre à jour Composer
- D'installer les dépendances Composer
- D'installer Popper.js & BootStrap
- De compiler les ressources JS / CSS pour l'utilisation de Bootstrap via WebPack-Encore
- De générer la migrations de la BDD
- De réaliser la migration en BDD
- D'écrire en BDD les données par défaut