apt update
apt upgrade
apt install npm

composer self-update
composer install

npm install popper.js --save
npm install bootstrap --save

composer run-script dev

php bin/console d:m:diff
php bin/console d:m:m
php bin/console d:f:l

echo "You can use the Symfony Application, written by Baptiste Saint-Pierre, and modified by Aymeric Leger Achard."