apt update && apt upgrade
apt install npm

composer self-update
composer install

npm install symfony/webpack-encore
npm install bootstrap --save-dev
npm install jquery @popperjs/core --save-dev
npm install postcss-loader@^7.0.0 --save-dev
npm install sass-loader@^14.0.0 sass --save-dev
npm run-script dev

php bin/console d:m:diff
php bin/console d:m:m